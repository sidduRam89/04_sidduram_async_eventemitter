const event = require('events');
const eventEmitter = new event.EventEmitter();
eventEmitter.on('getFile1',function(file1,text){
	console.log(file1+" : "+text);
})
eventEmitter.on('getFile2',function(file2,text){
	console.log(file2+" : "+text);
})
eventEmitter.on('getFile3',function(file3,text){
	console.log(file3+" : "+text);
})
eventEmitter.on("fileNotFound",function(){
	console.log("file not found");
});
function fakeAjax(url) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);
	setTimeout(function () {
		if(fake_responses[url]===undefined){
			eventEmitter.emit("fileNotFound");
		}
		if(url==="file1"){
			eventEmitter.emit("getFile1",url,fake_responses[url]);
		}
		if(url==="file2"){
			eventEmitter.emit("getFile2",url,fake_responses[url]);
		}
		if(url==="file3"){
			eventEmitter.emit("getFile3",url,fake_responses[url]);
		}
	}, randomDelay);
}

function getFile(file) {
	fakeAjax(file);
}

// request all files at once in "parallel"
getFile("file1");
getFile("file2");
getFile("file3");
getFile("file4");